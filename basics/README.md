# __

- Setup ESLint, Prettier in React Javascript
```
yarn add -D eslint-plugin-react \
     @babel/eslint-parser \
    eslint \
    eslint-config-prettier \
    eslint-plugin-prettier \
    prettier
```

- If using air-bnb
```
Yarn add -D  @babel/eslint-parser eslint eslint-config-airbnb eslint-config-prettier eslint-plugin-react eslint-plugin-import prettier pretty-quick
```

- @babel/eslint-parser not working --> use old version: babel-eslint instead of