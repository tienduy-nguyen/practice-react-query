module.exports = {
  parser: '@babel/eslint-parser',
  parserOptions: {
    requireConfigFile: false,
    ecmaVersion: 2020,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
  extends: [
    'prettier',
    // 'airbnb',
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:prettier/recommended',
  ],
  ignorePatterns: [
    '.eslintrc.js',
    '*.config.js',
    '*.json',
    '__test__',
    '**/setupTest.js',
    'jsconfig.json',
    '*.jsx',
  ],
  rules: {
    // Place to specify ESLint rules. Can be used to overwrite rules specified from the extended configs
    // 'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
    'react/jsx-filename-extension': [0],
    'no-use-before-define': [0],
    'react/prop-types': 'off',
    'react/no-unescaped-entities': 'off',
    'react/display-name': 'off',
    'no-return-assign': ['off'],
    'prefer-promise-reject-errors': ['off'],
    'no-shadow': 'off',
    'react/button-has-type': 'off',
  },
};
