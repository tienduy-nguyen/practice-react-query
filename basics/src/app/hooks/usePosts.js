import { useQuery } from 'react-query';

export function usePosts() {
  return useQuery('posts', async () => {
    const res = await fetch('https://jsonplaceholder.typicode.com/posts');
    const data = await res.json();
    return data;
  });
}
