import { useQuery } from 'react-query';

async function getPostById(id) {
  const res = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`);
  return await res.json();
}

export function usePost(postId) {
  return useQuery(['post', postId], () => getPostById(postId), {
    enabled: !!postId,
  });
}
